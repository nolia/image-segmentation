
const int IMAGE_OUT_TYPE_BMP = 0;
const int IMAGE_OUT_TYPE_PNG = 1;
const int IMAGE_OUT_TYPE_JPG = 2;

extern "C" __declspec(dllexport) int add(int a , int b);

extern "C" __declspec(dllexport) int segmentImageFile(
    const char* inImage, 
    const float k,
    const int min_size, 
    const float sigma, 
    const char* outImage, 
    const int outType = 0 
    );
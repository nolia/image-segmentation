#include "imdl.h"
#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "image.h"
#include "misc.h"
#include "pnmfile.h"
#include "segment-image.h"
#include "images_gdi.h"
#pragma once

extern "C" __declspec(dllexport) int add(int a, int b){
    std::cout << "Adding " << a <<" " << b << "\n"; 
    return a + b;
}

extern "C" __declspec(dllexport) int segmentImageFile(
    const char* inImage, 
    const float k, 
    const int min_size, 
    const float sigma, 
    const char* outImage, 
    const int outType   )
{    
    std::cout << "loading input image. \n";
    
    size_t origsize = strlen(inImage) + 1;
    const size_t newsize = 256;
    size_t convertedChars = 0;
    WCHAR in_wchar[newsize];
    WCHAR out_wchar[newsize];
    
    mbstowcs_s(&convertedChars, in_wchar, origsize, inImage, _TRUNCATE);
    std::wcout << " in file: " << in_wchar << std::endl;
    origsize = strlen(outImage) + 1;
    mbstowcs_s(&convertedChars, out_wchar, origsize, outImage, _TRUNCATE);

    image<rgb>* input = loadBMP( in_wchar ); 
    std::cout << " processing ... \n";
    int num_css;
    image<rgb>* seg = segment_image(input, sigma, k, min_size, &num_css);
    std::cout << "saving...";
    std::cout << "got " << num_css << " components\n";
    saveBMP(seg, out_wchar, outType);

    return num_css;
}
#include <cstdio>
#include <cstdlib>
#include "image.h"
#include "misc.h"
#include "pnmfile.h"
#include "segment-image.h"
#include "images_gdi.h"
#include "imdl.h"

#pragma once

int myMain();

int main(int argc, char **argv){
    int num_ccs; 
    float sigma = 0.5;
    float k = 500;
    int min_size = 200;
    num_ccs = segmentImageFile(
        "D:\\test_img\\grain.png",
        k,
        min_size,
        sigma,
        "D:\\test_img\\grain_out.jpg",
        IMAGE_OUT_TYPE_JPG
        );
    printf("result out components:%d \n", num_ccs);
    system("PAUSE");
    //return myMain();
    return 0;
}

int mainPPM(int argc, char **argv) {
  if (argc != 6) {
    fprintf(stderr, "usage: %s sigma k min input(ppm) output(ppm)\n", argv[0]);
    return 1;
  }
  
  float sigma = atof(argv[1]);
  float k = atof(argv[2]);
  int min_size = atoi(argv[3]);
	
  printf("loading input image.\n");
  image<rgb> *input = 
      //loadBMP((const WCHAR*) argv[4]); 
      loadPPM(argv[4]);
	
  printf("processing\n");
  int num_ccs; 
  image<rgb> *seg = segment_image(input, sigma, k, min_size, &num_ccs); 
  savePPM(seg, argv[5]);

  printf("got %d components\n", num_ccs);
  printf("done! uff...thats hard work.\n");

  return 0;
}

int myMain()
{
    printf("loading input image.\n");

    image<rgb> *input = loadBMP(L"D:\\test_img\\beach.jpg");    

    printf("processing\n");
    int num_ccs; 
    float sigma = 0.5;
    int k = 500;
    int min_size = 200;
    image<rgb> *seg = segment_image(input, sigma, k, min_size, &num_ccs); 
    //mainPPM(argc, argv);

    saveBMP(seg, L"D:\\test_img\\out_beach.png");
    savePPM(seg, "D:\\test_img\\out_beach.ppm");

    printf("got %d components\n", num_ccs);
    printf("done! uff...thats hard work.\n");

    delete input;
    delete seg;
    return 0;
}


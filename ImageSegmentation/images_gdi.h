#include <windows.h>
#include <gdiplus.h>
#include <stdio.h>
#include "misc.h"
#pragma comment (lib,"Gdiplus.lib")


using namespace Gdiplus;

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
    UINT  num = 0;          // number of image encoders
    UINT  size = 0;         // size of the image encoder array in bytes

    ImageCodecInfo* pImageCodecInfo = NULL;

    GetImageEncodersSize(&num, &size);
    if(size == 0)
        return -1;  // Failure

    pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
    if(pImageCodecInfo == NULL)
        return -1;  // Failure

    GetImageEncoders(num, size, pImageCodecInfo);

    for(UINT j = 0; j < num; ++j)
    {
        if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
        {
            *pClsid = pImageCodecInfo[j].Clsid;
            free(pImageCodecInfo);
            return j;  // Success
        }    
    }

    free(pImageCodecInfo);
    return -1;  // Failure
}

image<rgb>* loadBMP(const WCHAR* name)
{
   // Initialize GDI+.
   GdiplusStartupInput gdiplusStartupInput;
   ULONG_PTR gdiplusToken;
   GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
   Bitmap* bmp = new Bitmap(name);
   int width = bmp->GetWidth();
   int height = bmp->GetHeight();
   BitmapData* bitmapData = new BitmapData;
   Rect* rect = new Rect(0, 0, width, height );
   
   image<rgb> *im = new image<rgb>(width, height);
   
   bmp->LockBits(
       rect,
       ImageLockModeRead,
       PixelFormat32bppARGB,
       bitmapData
       );
   UINT* pixels = (UINT*) bitmapData->Scan0;
   //scan pixels
   const UINT stride = bitmapData->Stride;
   for( UINT row = 0; row < height; ++row )
   {
       for ( UINT col = 0; col < width; ++col)
       {
          UINT pxl = pixels[row * bitmapData->Stride / 4 + col];
          rgb _rgb;
          _rgb.r = (pxl >> 16) & 0x000000ff ;
          _rgb.g = (pxl >>  8) & 0x000000ff ;
          _rgb.b = (pxl >>  0) & 0x000000ff ;
          imRef(im, col, row) = _rgb;
       }
       
   }
   
   bmp->UnlockBits(bitmapData);
   delete(bitmapData);
   delete(bmp);
   // deinit GDI+
   GdiplusShutdown(gdiplusToken);

   return im;
}

void saveBMP(image<rgb>* im, const WCHAR* filename)
{
   
    // Initialize GDI+.
   GdiplusStartupInput gdiplusStartupInput;
   ULONG_PTR gdiplusToken;
   GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
   const int width = im->width();
   const int height = im->height();
   const UINT pixelCount = width * height;
   Bitmap* bmp = new Bitmap(width, height );
   Rect* rect = new Rect(0, 0, width, height );
   BitmapData* bitmapData = new BitmapData;

   bmp->LockBits(
       rect,
       ImageLockModeWrite,
       PixelFormat32bppARGB,
       bitmapData
       );
   UINT* pixels = (UINT*) bitmapData->Scan0;
   //scan pixels
   const UINT stride = bitmapData->Stride;
   for( UINT row = 0; row < height; ++row )
   {
       for ( UINT col = 0; col < width; ++col)
       {
           UINT pxl;
           rgb _rgb = imRef(im, col, row);
           pxl = 0xff000000 
               | (UINT) _rgb.r << 16
               | (UINT) _rgb.g << 8
               | (UINT) _rgb.b << 0
               ;
           pixels[row * bitmapData->Stride / 4 + col] = pxl;
       }

   }
   CLSID clsId;
   GetEncoderClsid(L"image/png", &clsId);
   bmp->Save(filename, &clsId);

   delete bmp;
   delete bitmapData;
   // deinit GDI+
   GdiplusShutdown(gdiplusToken);
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace ImageSeg
{

    public partial class Form1 : Form
    {
        private string inImageFileName;

        public Form1()
        {
            InitializeComponent();
        }

        [DllImport("ImageDll.dll", CallingConvention=CallingConvention.Cdecl, EntryPoint="segmentImageFile")]
        public static extern int segmentImageFile(
            string inImage, 
            float k,
            int min_size, 
            float sigma, 
            string outImage, 
            int outType 
        );

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK
                && openFileDialog1.CheckFileExists )
            {
                inImageFileName = openFileDialog1.FileName;
                showImage(openFileDialog1.FileName);
            }
        }

        private void showImage(string fileName)
        {
            Bitmap bmp = new Bitmap(fileName);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.Image = bmp;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // segment image and save
            progressBar1.Visible = true;
            disableComponents();
            trySegmentImage();
            enableComponents();
        }

        private void enableComponents()
        {
            progressBar1.Visible = false;

            buttonOpen.Enabled = true;
            buttonSegment.Enabled = true;
        }
        private void disableComponents()
        {
            progressBar1.MarqueeAnimationSpeed = 50;
            progressBar1.Visible = true;

            buttonOpen.Enabled = false;
            buttonSegment.Enabled = false;

        }
        private string outFileName = "out.bmp";
        private int inIndex = 1;

        private void trySegmentImage()
        {
            if (inImageFileName == null)
            {
                MessageBox.Show("Choose file!");
                return;
            }
            // TODO in other thread
            outFileName = "out" + inIndex + ".bmp";
            inIndex++;
            
            float threasholdK = (float) numericThreshold.Value;
            int minSize = (int) numericMinSize.Value;
            float sigma = 0.1f * trackBarSigma.Value; 
            segmentImageFile(inImageFileName, 
                    threasholdK,
                    minSize, 
                    sigma, 
                    outFileName, 
                    0);
            showImage(outFileName);
         
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            for (int i = 1; i <= inIndex; ++i)
            {
                String outName = "out" + i + ".bmp";
                File.Delete(outName);
            }
        }

    }
}
